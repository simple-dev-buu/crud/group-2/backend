export class CreateEmployeeDto {
  roles: string;
  firstName: string;
  lastName: string;
  tel: string;
  gender: string;
  birthDate: string;
  qualification: string;
  moneyrate: number;
  minWorkingHour: number;
  startDate: string;
}
