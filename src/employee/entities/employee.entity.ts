import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  roles: string;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  tel: string;

  @Column()
  gender: string;

  @Column()
  birthDate: string;

  @Column()
  qualification: string;

  @Column()
  moneyrate: number;

  @Column()
  minWorkingHour: number;

  @Column()
  startDate: string;
}
