import { IsNotEmpty, Length } from 'class-validator';

export class CreateMemberDto {
  @IsNotEmpty()
  fullName: string;

  @IsNotEmpty()
  @Length(10)
  tel: string;

  point: number;

  @IsNotEmpty()
  birthDate: string;
}
