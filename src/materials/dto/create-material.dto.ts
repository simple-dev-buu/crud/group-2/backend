export class CreateMaterialDto {
  ingredientID: string;
  ingredientName: string;
  minimum: number;
  balance: number;
  unit: string;
  status: string;
}
