import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Material {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  ingredientID: string;

  @Column()
  ingredientName: string;

  @Column()
  minimum: number;

  @Column()
  balance: number;

  @Column()
  unit: string;

  @Column()
  status: string;
}
